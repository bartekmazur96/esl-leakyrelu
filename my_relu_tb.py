from myhdl import *
from my_relu import stateMode, my_relu
from axi.axis import Axis
from myhdl_sim.clk_stim import clk_stim
import os


inputData = [1, -2, 3, -2, 2, 2, 2, 1, 1, 3, 4, 1, -2, -3, -3, -5, 4, 5, 8, -4, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1] #dataDate + dataTarget





@block
def my_ralu_tb(vhdl_output_path=None):


    axis_raw = Axis(32)  # definiuje sygnal ktory mozne zmieniac wartosc 2^8 z 0 na 1 zwykly clock tylko sprzetowy
    axis_sum = Axis(32)

    reset = ResetSignal(0, active=0, async=False)
    # Clock
    clk = Signal(bool(0))  # ustawia poczatkowa wartosc zegara na false


    clk_gen = clk_stim(clk, period=10)  # zmienia okresowo stan zegara co 10ms

    @instance
    def reset_gen():
        reset.next = 0
        yield delay(54)
        yield clk.negedge  # zbocze opadajace
        reset.next = 1

    @instance
    def testbench():
        axis_raw.tvalid.next = 1
        # Write data to the peak detector
        i = 0
        yield reset.posedge
        while i < len(inputData):
            yield clk.negedge
            axis_raw.tvalid.next = 1
            axis_raw.tdata.next = inputData[i]
            if i == len(inputData) - 1:
                axis_raw.tlast.next = 1
            else:
                axis_raw.tlast.next = 0
            if axis_raw.tready == 1:
                i += 1
        # print(i)
        # print(inputData)
        yield clk.negedge
        axis_raw.tvalid.next = 0



    @instance
    def read_stim():
        yield reset.posedge
        yield clk.negedge

        axis_sum.tready.next = 1
        while True:
            yield clk.negedge
            if axis_sum.tlast == 1:
                break
        for i in range(1000):
            yield clk.negedge
        raise StopSimulation()


    uut = my_relu(clk, reset, axis_raw, axis_sum, alpha=50)

    if vhdl_output_path is not None:
        uut.convert(hdl='VHDL', path=vhdl_output_path)


    return instances()

if __name__ == '__main__':
    trace_save_path = '../out/testbench/'
    vhdl_output_path = '../out/vhdl/'
    os.makedirs(os.path.dirname(trace_save_path), exist_ok=True)
    os.makedirs(os.path.dirname(vhdl_output_path), exist_ok=True)

    tb = my_ralu_tb(vhdl_output_path)
    tb.config_sim(trace=True, directory=trace_save_path, name='my_relu_tb')
    tb.run_sim()



