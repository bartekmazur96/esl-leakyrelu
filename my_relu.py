from myhdl import block, modbv, always_seq, Signal, intbv, enum, instances

stateMode = enum('COLLECT', 'COUNTT', 'OUTT', 'NOP')


@block
def my_relu(clk, reset, axis_s_raw, axis_m_relu, alpha=100):

    state = Signal(stateMode.COLLECT)
    COLLECTCounter = Signal(modbv(0, min=0, max=32 * 2))
    register_values_temp = [Signal(modbv(0, min=0, max=2 ** 32)) for i in range(32)]
    result_vec =[Signal(modbv(0, min=0, max=2 ** 32)) for i in range(32)]
    licznik1 = Signal(modbv(0, min=0, max=2 ** 32))
    licznik2 = Signal(modbv(0, min=0, max=2 ** 32))



    @always_seq(clk.posedge, reset=reset)
    def relu_proc():
        axis_m_relu.tvalid.next = 0
        axis_m_relu.tlast.next = 0
        axis_s_raw.tready.next = 1
        if state == stateMode.COLLECT:
            if axis_s_raw.tvalid == 1:  # saving data input
                register_values_temp[COLLECTCounter].next = axis_s_raw.tdata  # saving data input
                COLLECTCounter.next = COLLECTCounter + 1
                
                if axis_s_raw.tlast == 1:
                    state.next = stateMode.COUNTT



        elif state == stateMode.COUNTT:

            licznik1.next = licznik1 + 1

            if licznik1 == 30:
                state.next = stateMode.OUTT
            else:
                if register_values_temp[licznik1] > 0:
                    result_vec[licznik1].next = register_values_temp[licznik1] * alpha
                    axis_m_relu.tvalid.next = 1
                    axis_m_relu.tdata.next = result_vec[licznik1].next
                    state.next = stateMode.NOP
                elif register_values_temp[licznik1] < 0:
                    result_vec[licznik1].next = register_values_temp[licznik1]
                    state.next = stateMode.NOP
                    



        elif state == stateMode.NOP:
            axis_m_relu.tvalid.next = 1
            axis_m_relu.tlast.next = 1

            state.next = stateMode.COUNTT

        elif state == stateMode.OUTT:
            

            axis_m_relu.tlast.next = 1
            if axis_m_relu.tready == 1:
                state.next = stateMode.COLLECT



        else:
            print('nop')



    return instances()
